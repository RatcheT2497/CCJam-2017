local attributerenderer = {
	surface = nil;
	buffer = {};
	attribute_buffer = {};
	
	set_pixel = function(self, x, y, toggle)
		self.surface:drawPixel(x, y, toggle and self.attribute_buffer[(math.floor(x / 2) + math.floor(y / 3) * (self.surface.width / 2)) + 1] or colors.black);
	end;
	
	load_colormap = function(path, transparent)
		transparent = transparent or false;

		local lines = {}

		local file = fs.open(path, "r");
		local line = file.readLine();
		local width, height = 0, 0;
		while line ~= nil do
			width = math.max(width, #line);
			lines[#lines + 1] = line;
			line = file.readLine();
		end
		file.close();
		local image_height_half
		if not transparent then
			image_height_half = #lines / 2
			height = image_height_half;
		else
			height = #lines
		end

		local bcl, tcl = {}, {}
		
		local bc, tc = {}, {};
		if not transparent then
			for i = 1, image_height_half do
				local line = lines[i]
				for w = 1, #line do
					local col = 2 ^ tonumber(line:sub(w, w), 16);
					bc[#bc + 1] = col;
				end
			end
			
			for i = image_height_half + 1, 2 * image_height_half do
				local line = lines[i]
				for w = 1, #line do
					local col = 2 ^ tonumber(line:sub(w, w), 16);	
					tc[#tc + 1] = col;
				end
			end
		else
			for i = 1, height do
				local line = lines[i]
				for w = 1, #line do
					local col = 2 ^ tonumber(line:sub(w, w), 16);
					bc[#bc + 1] = col;
					tc[#tc + 1] = col;
				end
			end
		end
		local t = {};
		t.width = width;
		t.height = height;
		t.back_color = bc;
		t.text_color = tc;
		return t;
	end;
	
	draw_colormap = function(self, x, y, colormap)
		for i = 1, colormap.height do
			for w = 1, colormap.width do
				if x + w >= 1 and x + w <= (self.surface.width / 2) then
					local color_index = ((w - 1) + (i - 1) * colormap.width) + 1;
					
					local buffer_index = ((x + w - 1) + (y + i - 1) * math.floor(self.surface.width / 2)) * 2;
					if colormap.back_color[color_index] then
						self.attribute_buffer[buffer_index + 1] = colormap.back_color[color_index];
					end
					if colormap.text_color[color_index] ~= colormap.back_color[color_index] then
						self.attribute_buffer[buffer_index + 2] = colormap.text_color[color_index];
					end
				end
			end
		end

	end;
	clear_buffer = function(self)
		for i = 1, (self.surface.width / 2) * (self.surface.height / 3) * 2, 2 do
			self.attribute_buffer[i] = colors.white;
			self.attribute_buffer[i + 1] = colors.black;
		end
	end;
	set_attribute = function(self, x, y, back_color, text_color)
		local index = (x + y * math.floor(self.surface.width / 2)) * 2;
		self.attribute_buffer[index + 1] = back_color or colors.white;
		self.attribute_buffer[index + 2] = text_color or colors.black;
	end;

	render = function(self)
		for i = 0, self.surface.width - 1 do
			for w = 0, self.surface.height - 1 do
				local index = ((i + w * self.surface.width) * 3) + 1;
				local pixb, pixt, pixc = self.surface.buffer[index], self.surface.buffer[index + 1];
				
				local attrx, attry = math.floor(i / 2), math.floor(w / 3);
				local attrindex = (2 * (attrx + attry * (self.surface.width / 2))) + 1;

				local attrbackcolor, attrtextcolor = self.attribute_buffer[attrindex], self.attribute_buffer[attrindex + 1];
				if pixb == colors.white then
					self.surface.buffer[index] = attrbackcolor;
				elseif pixb == colors.black then
					self.surface.buffer[index] = attrtextcolor;
				--else
				--	self.surface.buffer[index] = attrbackcolor;
				end
			end
		end
	end;	
}

return {
	new = function(surface)
		local v = {};
		v.surface = surface;
		v.buffer = {};
		v.attribute_buffer = {};

		for i = 1, surface.width * surface.height do
			v.buffer[i] = false;
		end
		
		for i = 1, (surface.width / 2) * (surface.height / 3) * 2, 2 do
			v.attribute_buffer[i] = colors.white;
			v.attribute_buffer[i + 1] = colors.black;
		end
		
		return setmetatable(v, {__index = attributerenderer});
	end
};