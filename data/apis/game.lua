local flr = math.floor;
local lerp = function(v0, v1, t)
	return (1 - t) * v0 + t * v1;
end;
local in_table = function(t, value)
	for _, v in pairs(t) do
		if v == value then return true; end
	end
	return false;
end;
local gray_light_gradient = {colors.black, colors.gray, colors.lightGray, colors.white, colors.white};
local green_blink = {colors.green, colors.lime, colors.white, colors.lime};
local point_in_rect= function(px, py, rx, ry, rw, rh)
	return  (px >= rx and px < rx + rw) and
			(py >= ry and py < ry + rh)
end
local map = {
	entities = {};
	width = 11;
	frogs_saved = 0;
	frogs_to_save = 0;

	current_segment = 1;
	data_segments = {};
	data_segments_solid = {};
	tile_images = {};
	tile_colormaps = {};
	surface = nil;
	attribute_renderer = nil;
	tileset_paths = {};
	frog = nil;
	add_entity = function(self, ent, segment)
		self.entities[segment][#self.entities + 1] = ent;
	end;
	get_solid = function(self, x, y)
		return self.data_segments_solid[self.current_segment][(x + y * self.width) + 1];
	end;
	init = function(self, working_path, surface_api, attribute_renderer)
		self.surface = surface;
		self.attribute_renderer = attribute_renderer;
		self.blank_colormap = self.attribute_renderer.load_colormap(working_path .. "/data/gfx/tiles/blank_cm.nfp");
		for i = 1, #self.tileset_paths, 2 do
			self.tile_images[flr((i - 1) / 2) + 1] = surface_api.load(working_path .. self.tileset_paths[i]);
			self.tile_colormaps[flr((i - 1) / 2) + 1] = self.attribute_renderer.load_colormap(working_path .. self.tileset_paths[i + 1]);
		end

		for i = 1, #self.segment_paths do
			local seg = dofile(working_path .. "/data/maps/segments/" .. self.segment_paths[i]);
			self.data_segments[i] = seg.tile_data;
			for w = 1, #seg.entity_data do
				local e = {}
				e.x = seg.entity_data[w].x;
				e.y = seg.entity_data[w].y;
				e.type = seg.entity_data[w].type;
				e.variables = seg.entity_data[w].variables;
				setmetatable(e, {__index = self.entity_types[e.type]})
				table.insert(self.entities[i], e);
			end
			self.data_segments_solid[i] = {};
			for w = 1, #self.data_segments[i] do
				self.data_segments_solid[i][w] = in_table(self.solid_tiles, self.data_segments[i][w]);
			end
		end
		for i = 1, #self.segment_paths do
			for _, v in pairs(self.entities[i]) do
				v:init(working_path, surface_api, attribute_renderer);
			end
		end
	end;
	update = function(self)
		local found = false;
		for k, v in pairs(self.entities[self.current_segment]) do
			if self.frog.die then break end
			if v.type == "log" or v.type == "turtle" then
				if point_in_rect(self.frog.x, self.frog.y, v.x, v.y, v.variables.count * 8, 4) or 
					point_in_rect(self.frog.x + 8, self.frog.y, v.x, v.y, v.variables.count * 8, 4) then
					found = true;
					self.entities[self.current_segment][k].follow = self.frog;
					self.frog.following = v;
				end
			elseif v.type == "car" then
				if not self.frog.move then
					if v.variables.car_index == 0 then
						if point_in_rect(self.frog.x, self.frog.y, v.x, v.y, 16, 4) or 
							point_in_rect(self.frog.x + 8, self.frog.y, v.x, v.y, 16, 4) then
							self.frog.die = true;
							break;
						end				
					else
						if point_in_rect(self.frog.x, self.frog.y, v.x, v.y, 8, 4) or 
							point_in_rect(self.frog.x + 8, self.frog.y, v.x, v.y, 8, 4) then
							self.frog.die = true;
							break;
						end				
					end
				end
			end
		end
		if self.frog.following then
			if not found then 
				self.frog.following = false; 
			end
		end
		for _, v in pairs(self.entities[self.current_segment]) do
			v:update();
		end
	end;
	handle_events = function(self, e)
		for _, v in pairs(self.entities[self.current_segment]) do
			v:handle_events(e);
		end;
	end;
	offset = 0;
	render = function(self, surface, camera_y)
		local segment = self.data_segments[self.current_segment];
		for i = 0, self.width - 1 do
			for w = 0, (#segment / self.width) - 1 do
				local t = segment[(i + w * self.width) + 1];
				local x = i * 8;
				local y = (w * 9) - camera_y;
				if t > 0 then
					self.attribute_renderer:draw_colormap(flr(x / 2) + (self.offset / 2), flr((y) / 3), self.tile_colormaps[t]);
					surface:fillRect(x + self.offset, y, 8, 9, colors.black);
					surface:drawSurface(self.tile_images[segment[(i + w * self.width) + 1]], x + self.offset, y);
				elseif t == 0 then
					self.attribute_renderer:draw_colormap(flr(x / 2) + (self.offset / 2), flr((y) / 3), self.blank_colormap);
					surface:fillRect(x + self.offset, y, 8, 9, colors.black);				
				end
			end
		end
		for _, v in pairs(self.entities[self.current_segment]) do
			v:render(surface, camera_y);
		end
	end;
}
local point_in_rect = function(px, py, rx, ry, rw, rh)
	return  (px >= rx and px < rx + rw) and
			(py >= ry and py < ry + rh)
end;
local frog = {
	type = "frog";
	editor = false;
	visible = true;
	surface = nil;
	attribute_renderer = nil;

	images = {};
	color_map = {};
	
	left = false;
	right = false;
	up = false;
	down = false;
	
	move = false;
	direction = 2;
	steps = 0;

	x = 40;
	y = 45;
	following = false;
	selected_tile = 5;
	die = false;
	on_land = function(self, tile)
		-- +1 frog
		if tile == 1 then
			map.frogs_saved = map.frogs_saved + 1;
			if map.frogs_saved == map.frogs_to_save then 
				self.game:win()
			end
			self.game:first_segment();
			map.data_segments[map.current_segment][(flr(self.x / 8) + flr(self.y / 9) * map.width) + 1] = 2;
			self.visible = false;
			self.y = self.start_y;
		end
		-- go to next segment, if not on last segment
		if self.y <= 0 then
			self.game:next_segment();
		end
		-- die
		for i = 1, #map.deadly_tiles do 
			if tile == map.deadly_tiles[i] then 
				local candie = true;
				if self.following then	
					if self.following.type == "turtle" then
						if self.following.state ~= 2 then
							candie = false;
						end
					else
						candie = false;
					end
				end
					
				self.die = candie; 
				break; 
				
			end
		end
	
	end;
	
	init = function(self, working_path, surface_api, attribute_renderer)
		self.surface = surface;
		self.attribute_renderer = attribute_renderer;
		
		self.color_map[1] = self.attribute_renderer.load_colormap(working_path .. "/data/gfx/frog/frog_cm.nfp", true);
		self.color_map[2] = self.attribute_renderer.load_colormap(working_path .. "/data/gfx/frog/frog_move_hor_cm.nfp", true);
		self.color_map[3] = self.attribute_renderer.load_colormap(working_path .. "/data/gfx/frog/frog_move_ver_cm.nfp", true);
		
		self.images[0] = surface_api.load(working_path .. "/data/gfx/frog/frog_right.nfp")
		self.images[1] = surface_api.load(working_path .. "/data/gfx/frog/frog_left.nfp")
		self.images[2] = surface_api.load(working_path .. "/data/gfx/frog/frog_up.nfp")		
		self.images[3] = surface_api.load(working_path .. "/data/gfx/frog/frog_down.nfp")
		
		self.images[4] = surface_api.load(working_path .. "/data/gfx/frog/frog_move_right.nfp")
		self.images[5] = surface_api.load(working_path .. "/data/gfx/frog/frog_move_left.nfp")
		self.images[6] = surface_api.load(working_path .. "/data/gfx/frog/frog_move_up.nfp")		
		self.images[7] = surface_api.load(working_path .. "/data/gfx/frog/frog_move_down.nfp")

		self.rip_image = surface_api.load(working_path .. "/data/gfx/frog/rip.nfp");
		self.rip_colormap = self.attribute_renderer.load_colormap(working_path .. "/data/gfx/frog/rip_cm.nfp", true);
	end;

	render = function(self, surface, camera_y)
		if not self.editor then
			if self.die then
				self.attribute_renderer:draw_colormap(flr(self.x / 2), flr(self.y - camera_y) / 3, self.rip_colormap);
				surface:fillRect(self.x, self.y - camera_y, 8, 9, colors.black);
				surface:drawSurface(self.rip_image, self.x, self.y - camera_y);
			else
				if not self.visible then return end
				local cur_image = self.images[self.direction + (self.move and 4 or 0)];
				local cur_colormap = self.color_map[self.move and ((self.direction == 0 or self.direction == 1) and 2 or 3) or 1];
				self.attribute_renderer:draw_colormap(flr((self.x - (self.move and (self.direction == 1 and 3 or 0) or 0)) / 2), 
													  flr((self.y - camera_y - (self.move and (self.direction == 2 and 3 or 0) or 0)) / 3), 
													  cur_colormap);
				surface:fillRect(self.x - (self.move and (self.direction == 1 and 3 or 0) or 0), self.y - camera_y - (self.move and (self.direction == 2 and 3 or 0) or 0), cur_image.width, cur_image.height, colors.black);
				surface:drawSurface(cur_image, self.x - (self.move and (self.direction == 1 and 3 or 0) or 0), self.y - camera_y - (self.move and (self.direction == 2 and 3 or 0) or 0));
			end
		else
			local t = self.selected_tile;
			if t == 0 then 
				surface:fillRect(flr(self.x), self.y, 8, 9, colors.black);
				return 
			end
			self.attribute_renderer:draw_colormap(flr(flr(self.x) / 2), flr((self.y) / 3), map.tile_colormaps[t]);
			surface:fillRect(flr(self.x), self.y, 8, 9, colors.black);
			surface:drawSurface(map.tile_images[self.selected_tile], self.x, self.y);
		end
	end;
	
	update = function(self)
		if self.x > 72 then self.x = self.x - math.abs(self.x - 72) end
		if self.x < 8 then self.x = self.x + math.abs(8 - self.x) end
		
		if self.game.camera_y ~= 0 then return end
		if self.die then return end
		if self.move then
			if self.steps ~= 0 then
				if self.direction == 0 then
					self.x = self.x + 2;
				elseif self.direction == 1 then
					self.x = self.x - 2;
				elseif self.direction == 2 then
					self.y = self.y - 3;
				elseif self.direction == 3 then
					self.y = self.y + 3;
				end
				self.steps = self.steps - ((self.direction < 2) and ((self.steps > 0) and 2 or -2) or ((self.steps > 0) and 3 or -3));
			else
				self:on_land(map.data_segments[map.current_segment][(flr(self.x / 8) + flr(self.y / 9) * map.width) + 1])
				self.steps = 0;
				self.move = false;
			end
		end
		if self.following then
			if (self.x % 8) == 0 then
				self:on_land(map.data_segments[map.current_segment][(flr(self.x / 8) + flr(self.y / 9) * map.width) + 1])
			end
		end
	end;
	
	handle_events = function(self, e)
		if self.game.camera_y ~= 0 then return end
		if e[1] == "key" then
			if not self.move then
				if e[2] == keys.right then 
					if not self.editor then
						if self.x < 68 then
							self.move = true; 
							self.direction = 0; 
							self.steps = 8; 
						end
					else
						self.x = self.x + 8;
					end
				end
				if e[2] == keys.left then 
					if not self.editor then
						if self.x > 8 then
							self.move = true; 
							self.direction = 1; 
							self.steps = -8; 
						end
					else
						self.x = self.x - 8;
					end
				end
				if e[2] == keys.up then 
					local cango = true;
					if in_table(map.solid_tiles, map.data_segments[map.current_segment][(flr(self.x / 8) + (flr(self.y / 9) - 1) * map.width) + 1]) then 
						cango = false;
					end
					
					if not self.editor then
						if cango then
							self.move = true; 
							self.direction = 2; 
							self.steps = -9; 
						end
					else
						self.y = self.y - 9;
					end
				end
				if e[2] == keys.down then 
					if not self.editor then
						if self.y < self.start_y then
							self.move = true; 
							self.direction = 3; 
							self.steps = 9; 
						end
					else
						self.y = self.y + 9;
					end
				end
				if e[2] == keys.space then
					map.data_segments[map.current_segment][(flr(self.x / 8) + flr(self.y / 9) * map.width) + 1] = self.selected_tile;
				end
			end
			if self.editor then
				if e[2] == keys.w then
					self.selected_tile = self.selected_tile + 1;
					if self.selected_tile < 0 then self.selected_tile = #map.tile_images end
					if self.selected_tile > #map.tile_images then self.selected_tile = 0 end
				end
				if e[2] == keys.s then
					self.selected_tile = self.selected_tile - 1;
					if self.selected_tile < 0 then self.selected_tile = #map.tile_images end
					if self.selected_tile > #map.tile_images then self.selected_tile = 0 end
				end
				if e[2] == keys.f1 then
					local file = fs.open(map.segment_paths[map.current_segment], "w");
					file.write("return " .. textutils.serialize(map.data_segments[map.current_segment]));
					file.close();
				end
			end
		end
	end;
}

local STATE_ENUM = {	
	TITLESCREEN = 0;
	GAME = 1;
	GAME_OVER = 2;
	WIN = 3;
}
local states = {
	[STATE_ENUM.TITLESCREEN] = {
		game = nil;
		gui = nil;

		selected = 1;

		text_colorindex_1 = 4;
		text_colorindex_2 = 1;
		flash = 0;
		
		init = function(self, working_path, surface_api, attribute_renderer, gui, game)
			self.working_path = working_path;

			self.game = game;
			self.gui = gui;
			
			self.gui.elements = {};

			self.gui:add_label("frogger_title", "Frogger", 1, 2, colors.black, colors.lime);
			self.gui:add_label("frogger_subtitle", ": ComputerCraft Edition", 9, 2, colors.black, colors.white);
				
			self.gui:add_rectangle("menu_rectangle_1", -13, 6, 13, 1, colors.lightGray);
			self.gui:add_rectangle("menu_rectangle_2", -13, 7, 13, 1, colors.lightGray);

			self.gui:add_label("menu_1", "Start Game", 1, 6, nil, colors.white);
			self.gui:add_label("menu_2", "Quit Game", 1, 7, nil, colors.white);
		end;

		update = function(self)
			if not self.gui.elements["menu_1"] then return end
			if self.gui.fade then return end

			self.gui.elements["frogger_title"].textcolor = green_blink[flr(self.flash + 1)];
			self.gui.elements["menu_2"].textcolor = gray_light_gradient[flr(self.text_colorindex_1) + 1];
			self.gui.elements["menu_1"].textcolor = gray_light_gradient[flr(self.text_colorindex_2) + 1];
			self.gui.elements["menu_rectangle_2"].backcolor = gray_light_gradient[math.min(flr(self.text_colorindex_2) + 1, 3)];
			self.gui.elements["menu_rectangle_1"].backcolor = gray_light_gradient[math.min(flr(self.text_colorindex_1) + 1, 3)];
			self.flash = self.flash + 0.3;
			self.flash = self.flash % #green_blink;

			if self.selected == 1 then
				self.text_colorindex_1 = lerp(self.text_colorindex_1, #gray_light_gradient-1, 0.4);
				self.text_colorindex_2 = lerp(self.text_colorindex_2, 0, 					 0.4);

				self.gui.elements["menu_rectangle_1"].x = (lerp(self.gui.elements["menu_rectangle_1"].x, 0, 0.5));
				self.gui.elements["menu_rectangle_2"].x = (lerp(self.gui.elements["menu_rectangle_2"].x, -self.gui.elements["menu_rectangle_2"].width, 0.5));
			else
				self.text_colorindex_1 = lerp(self.text_colorindex_1, 0, 					 0.4);
				self.text_colorindex_2 = lerp(self.text_colorindex_2, #gray_light_gradient-1, 0.4);
				self.gui.elements["menu_rectangle_1"].x = (lerp(self.gui.elements["menu_rectangle_1"].x, -self.gui.elements["menu_rectangle_1"].width, 0.5));
				self.gui.elements["menu_rectangle_2"].x = (lerp(self.gui.elements["menu_rectangle_2"].x, 0, 0.5));
			end
		end;
		
		play_game = function(self)
			self.game.current_state = STATE_ENUM.GAME;
			self.game:init_state();
		end;
		quit_game = function(self)
			self.game.running = false;
		end;
		handle_events = function(self, e)
			if e[2] == keys.up then self.selected = 1; end
			if e[2] == keys.down then self.selected = 2; end
			if e[2] == keys.space then 
				if self.selected == 1 then
					self.gui:fade_to_black(self.play_game, {self}, 1.9, 2.0)
				elseif self.selected == 2 then
					self.gui:fade_to_black(self.quit_game, {self}, 1.9, 2.0)
				end
			end
		end;
		
		render = function(self, surface)
		end;
	};
	[STATE_ENUM.GAME] = {
		game = nil;
		camera_y = 0;
		scrolling = false;
		scroll_handle = nil;
		load_map = function(self, path)
			local m = dofile(path);
			
			map.tileset_paths = m.tileset_paths;
			map.segment_paths = m.data_segments;
			map.solid_tiles = m.solid_tiles;
			map.deadly_tiles = m.deadly_tiles;
			map.frogs_to_save = m.frogs_to_save;
			map.entity_types = m.entities;
			frog.game = self;
			frog.start_x = m.start_x * 8;
			frog.start_y = m.start_y * 9;
			frog.x = m.start_x * 8;
			frog.y = m.start_y * 9;
			map.frog = frog;
			for i = 1, #map.segment_paths do
				map.entities[i] = {};
				map:add_entity(frog, i);
			end
			map:init(self.working_path, self.surfapi, self.attribute_renderer);
		end;
		
		next_segment = function(self)
			self.scrolling = true;
			self.scroll_handle = function(self)
				if map.current_segment < #map.data_segments then
					map.current_segment = map.current_segment + 1;
					frog.y = frog.start_y;
					frog.move = false;
				end
			end
		end;
		first_segment = function(self)
			self.scrolling = true;
			self.scroll_handle = function(self)
				map.current_segment = 1;
				frog.x = frog.start_x;
				frog.y = frog.start_y;
				frog.visible = true;
				frog.move = false;
			end
		end;
		win = function(self)
			self.scrolling = true;
			self.scroll_handle = function(self)
				self.game.current_state = STATE_ENUM.WIN;
			end;
		end;
		init = function(self, working_path, surface_api, attribute_renderer, gui, game)
			self.working_path = working_path;
			self.surfapi = surface_api;
			self.attribute_renderer = attribute_renderer;
			self.game = game;
			self.gui = gui;
			self:load_map(self.working_path .. "/data/maps/map1.lua");
		end;

		update = function(self)
			map:update(self.camera_y);
			if self.scrolling then
				if self.camera_y >= -9 * 7 then 
					self.camera_y = self.camera_y - 3
				else
					if self.scroll_handle then self:scroll_handle(); end
					self.scrolling = false;
					self.camera_y = -self.camera_y
				end
			else
				if self.camera_y ~= 0 then 
					self.camera_y = self.camera_y + (self.camera_y > 0 and -3 or 3)
				end
			end
		end;

		handle_events = function(self, e)
			map:handle_events(e);
		end;

		render = function(self, surface)
			self.attribute_renderer:clear_buffer();

			map:render(surface, self.camera_y);
			frog:render(surface, self.camera_y);
			
			self.attribute_renderer:render();
			surface:fillRect(88, 0, surface.width - 88, surface.height, colors.black);
			
		end;
	};
	[STATE_ENUM.WIN] = {
		selected = 1;
		game = nil;
		gui = nil;
		text_colorindex_1 = 4;
		text_colorindex_2 = 1;
		restart_game = function(self)
			self.game.current_state = STATE_ENUM.GAME;
			self.game:init_state();
		end;
		quit_game = function(self)
			self.game.running = false;
		end;
		init = function(self, working_path, surface_api, attribute_renderer, gui, game)
			self.working_path = working_path;

			self.game = game;
			self.gui = gui;
			
			self.gui.elements = {};
		
			self.gui:add_label("win_label", "You got all the frogs to their lilypads!", 2, 2, colors.black, colors.white);
			
			self.gui:add_rectangle("restart_rectangle", 0, 5, 14, 1, colors.lightGray);
			self.gui:add_rectangle("quit_rectangle"   , 0, 6, 14, 1, colors.lightGray);
	
			self.gui:add_label("restart_button", "Restart Game", 1, 5, nil, colors.white);
			self.gui:add_label("quit_button", "Quit", 1, 6, nil, colors.white);
		end;
		update = function(self)
			if not self.gui.elements["restart_button"] then return end
			if self.gui.fade then return end

			self.gui.elements["restart_button"].textcolor = gray_light_gradient[flr(self.text_colorindex_2) + 1];
			self.gui.elements["quit_button"].textcolor = gray_light_gradient[flr(self.text_colorindex_1) + 1];
			self.gui.elements["restart_rectangle"].backcolor = gray_light_gradient[math.min(flr(self.text_colorindex_1) + 1, 3)];
			self.gui.elements["quit_rectangle"].backcolor = gray_light_gradient[math.min(flr(self.text_colorindex_2) + 1, 3)];

			if self.selected == 1 then
				self.text_colorindex_1 = lerp(self.text_colorindex_1, #gray_light_gradient-1, 0.4);
				self.text_colorindex_2 = lerp(self.text_colorindex_2, 0, 					 0.4);

				self.gui.elements["restart_rectangle"].x = (lerp(self.gui.elements["restart_rectangle"].x, 1, 0.5));
				self.gui.elements["quit_rectangle"].x = (lerp(self.gui.elements["quit_rectangle"].x, -self.gui.elements["quit_rectangle"].width, 0.5));
			else
				self.text_colorindex_1 = lerp(self.text_colorindex_1, 0, 					 0.4);
				self.text_colorindex_2 = lerp(self.text_colorindex_2, #gray_light_gradient-1, 0.4);
				self.gui.elements["restart_rectangle"].x = (lerp(self.gui.elements["restart_rectangle"].x, -self.gui.elements["restart_rectangle"].width, 0.5));
				self.gui.elements["quit_rectangle"].x = (lerp(self.gui.elements["quit_rectangle"].x, 1, 0.5));
			end
		end;
		handle_events = function(self, e)
			if e[2] == keys.up then self.selected = 1; end
			if e[2] == keys.down then self.selected = 2; end
			if e[2] == keys.space then 
				if self.selected == 1 then

					self.gui:fade_to_black(self.restart_game, {self}, 1.9, 2.0)
				elseif self.selected == 2 then
					self.gui:fade_to_black(self.quit_game, {self}, 1.9, 2.0)
				end
			end
		end;
		render = function(self, surface)
		end;
	}
}

local game = {
	working_path = "";
	current_state = STATE_ENUM.TITLESCREEN;
	timer_speed = 0.05;
	running = true;

	gui = nil;
	surfapi = nil;
	surface = nil;

	attribute_renderer_api = nil;
	attribute_renderer = nil;

	camera_y = -3;

	init = function(self, surface)
		self.surface = surface;
		self.width = width;
		self.height = height;
		self.surfapi = dofile(self.working_path .. "/data/apis/surface.lua");
		self.attribute_renderer_api = dofile(self.working_path .. "/data/apis/attributerenderer.lua");
		self.attribute_renderer = self.attribute_renderer_api.new(surface);
		self:init_state();
	end;
	
	init_state = function(self)
		states[self.current_state]:init(self.working_path, self.surfapi, self.attribute_renderer, self.gui, self);
	end;	
	
	update = function(self, gui)
		states[self.current_state]:update(gui);
	end;
	
	handle_events = function(self, e)
		if e[1] == "key" then
			if e[2] == keys.q then self.running = false; end		
		end
		states[self.current_state]:handle_events(e);
	end;

	render = function(self)
		states[self.current_state]:render(self.surface);
	end;		
}

return function(working_path, gui, surface)
	game.working_path = working_path;
	game.gui = gui;
	game:init(surface);
	return game;
end