return { 
	tile_data = {
	  6,
	  6,
	  1,
	  6,
	  6,
	  1,
	  6,
	  6,
	  1,
	  6,
	  6,
	  4,
	  4,
	  4,
	  4,
	  4,
	  4,
	  4,
	  4,
	  4,
	  4,
	  4,
	  4,
	  4,
	  4,
	  4,
	  4,
	  4,
	  4,
	  4,
	  4,
	  4,
	  4,
	  4,
	  4,
	  4,
	  4,
	  4,
	  4,
	  4,
	  4,
	  4,
	  4,
	  4,
	  4,
	  4,
	  4,
	  4,
	  4,
	  4,
	  4,
	  4,
	  4,
	  4,
	  4,
	  3,
	  3,
	  3,
	  3,
	  3,
	  3,
	  3,
	  3,
	  3,
	  3,
	  3,
	  3,
	  3,
	  3,
	  3,
	  3,
	  3,
	  3,
	  3,
	  3,
	  3,
	  3,
	};
	entity_data = {
		{
			type = "log";
			x = 0;
			y = 9;
			variables = {
				count = 4;
				speed = 1;
			};
		};
		{
			type = "log";
			x = 64;
			y = 9;
			variables = {
				count = 4;
				speed = 1;
			};
		};
		{
			type = "turtle";
			x = 0;
			y = 18;
			variables = {
				count = 2;
				speed = 3;
			};
		};
		{
			type = "turtle";
			x = 64;
			y = 18;
			variables = {
				count = 2;
				speed = 3;
			};
		};
		{
			type = "log";
			x = 0;
			y = 27;
			variables = {
				count = 4;
				speed = 2;
			};
		};
		{
			type = "turtle";
			x = 0;
			y = 36;
			variables = {
				count = 3;
				speed = 1;
			};
		};
		{
			type = "turtle";
			x = 64;
			y = 36;
			variables = {
				count = 3;
				speed = 1;
			};
		};
		
	};
}