return {
	tile_data = {
		3,
		3,
		3,
		3,
		3,
		3,
		3,
		3,
		3,
		3,
		3,
		0,
		5,
		0,
		5,
		0,
		5,
		0,
		5,
		0,
		5,
		0,
		5,
		0,
		5,
		0,
		5,
		0,
		5,
		0,
		5,
		0,
		5,
		0,
		5,
		0,
		5,
		0,
		5,
		0,
		5,
		0,
		5,
		0,
		0,
		0,
		0,
		0,
		0,
		0,
		0,
		0,
		0,
		0,
		0,
		3,
		3,
		3,
		3,
		3,
		3,
		3,
		3,
		3,
		3,
		3,
		3,
		3,
		3,
		3,
		3,
		3,
		3,
		3,
		3,
		3,
		3,
	};
	entity_data = {
		{
			type = "car";
			x = 0;
			y = 9;
			variables = {
				car_index = 0;
			};
		};
		{
			type = "car";
			x = 4*8;
			y = 9;
			variables = {
				car_index = 0;
			};
		};
		{
			type = "car";
			x = 0;
			y = 18;
			variables = {
				car_index = 1;
			};
		};
		{
			type = "car";
			x = 8;
			y = 27;
			variables = {
				car_index = 2;
			};
		};
		{
			type = "car";
			x = 24;
			y = 27;
			variables = {
				car_index = 2;
			};
		};
		{
			type = "car";
			x = 0;
			y = 36;
			variables = {
				car_index = 3;
			};
		};
		{
			type = "car";
			x = 32;
			y = 36;
			variables = {
				car_index = 3;
			};
		};
		{
			type = "car";
			x = 64;
			y = 36;
			variables = {
				car_index = 3;
			};
		};
	};
}