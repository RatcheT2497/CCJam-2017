return {
	start_x = 5;
	start_y = 5;
	frogs_to_save = 3;
	data_segments = {
		"m1_road.lua";
		"m1_river.lua";
	};
	tileset_paths = {
		"/data/gfx/tiles/lilypad.nfp";
		"/data/gfx/tiles/lilypad_cm.nfp";

		"/data/gfx/tiles/lilypad_frog.nfp";
		"/data/gfx/tiles/lilypad_frog_cm.nfp";

		"/data/gfx/tiles/grass.nfp";
		"/data/gfx/tiles/grass_cm.nfp";

		"/data/gfx/tiles/water_norm.nfp";
		"/data/gfx/tiles/water_cm.nfp";

		"/data/gfx/tiles/road.nfp";
		"/data/gfx/tiles/road_cm.nfp";

		"/data/gfx/tiles/trees.nfp";
		"/data/gfx/tiles/trees_cm.nfp";
	};
	entities = {
		["car"] = {
			visible = true;
			color_maps = {};
			images = {};
			init = function(self, working_path, surface_api, attribute_renderer)
				self.surface = surface;
				self.attribute_renderer = attribute_renderer;
				if self.variables.car_index == 0 then
					self.color_maps[1] = self.attribute_renderer.load_colormap(working_path .. "/data/gfx/entities/car_1_cm.nfp", true);
					self.color_maps[2] = self.attribute_renderer.load_colormap(working_path .. "/data/gfx/entities/car_2_cm.nfp", true);
					self.images[1] = surface_api.load(working_path .. "/data/gfx/entities/car_1.nfp");
					self.images[2] = surface_api.load(working_path .. "/data/gfx/entities/car_2.nfp");
				elseif self.variables.car_index == 1 then
					self.color_maps[1] = self.attribute_renderer.load_colormap(working_path .. "/data/gfx/entities/car_1_cm.nfp", true);
					self.images[1] = surface_api.load(working_path .. "/data/gfx/entities/car_1.nfp");					
				elseif self.variables.car_index == 2 then
					self.color_maps[1] = self.attribute_renderer.load_colormap(working_path .. "/data/gfx/entities/car_1_cm.nfp", true);
					self.images[1] = surface_api.load(working_path .. "/data/gfx/entities/car_1.nfp");					
				elseif self.variables.car_index == 3 then
					self.color_maps[1] = self.attribute_renderer.load_colormap(working_path .. "/data/gfx/entities/car_1_cm.nfp", true);
					self.images[1] = surface_api.load(working_path .. "/data/gfx/entities/car_1.nfp");					
				end
			end;
		
			render = function(self, surface, camera_y)
				if not self.visible then return end
				
				if self.variables.car_index == 0 then
					for i = 1, #self.images do
						self.attribute_renderer:draw_colormap(math.floor((self.x + ((i - 1) * 8)) / 2) - 1, 
															  math.floor((self.y - camera_y) / 3) - 1, 
															  self.color_maps[i]);
						surface:fillRect(self.x + ((i - 1) * 8), self.y - camera_y, self.images[i].width, self.images[i].height, colors.black);
						surface:drawSurface(self.images[i], self.x + ((i - 1) * 8), self.y - camera_y);
					end
				else
					self.attribute_renderer:draw_colormap(math.floor((self.x / 2) - 1), 
														  math.floor((self.y - camera_y) / 3) - 1, 
														  self.color_maps[1]);
					surface:fillRect(self.x, self.y - camera_y, self.images[1].width, self.images[1].height, colors.black);
					surface:drawSurface(self.images[1], self.x, self.y - camera_y);
				end
			end;
			
			update = function(self)
				if self.variables.car_index == 0 then
					self.x = self.x - 2;
				elseif self.variables.car_index == 1 then
					self.x = self.x + 3;
				elseif self.variables.car_index == 2 then
					self.x = self.x - 2;
				elseif self.variables.car_index == 3 then
					self.x = self.x + 1;
				end
				if self.x >= 88 then self.x = -16 end
				if self.x < -16 then self.x = 88 end
			end;
			
			handle_events = function(self, e)
				
			end
		};
		["log"] = {
			variables = { speed = 0; count = 0; };
			follow = nil;
			init = function(self, working_path, surface_api, attribute_renderer)
				self.surface = surface;
				self.attribute_renderer = attribute_renderer;
				self.color_map = self.attribute_renderer.load_colormap(working_path .. "/data/gfx/entities/log_cm.nfp", true);
				self.image = surface_api.load(working_path .. "/data/gfx/entities/log.nfp");
			end;
		
			render = function(self, surface, camera_y)
				for i = 1, self.variables.count do
					self.attribute_renderer:draw_colormap(math.floor((self.x + (i - 1) * 8) / 2), 
														  math.floor((self.y - camera_y) / 3), 
														  self.color_map);

					surface:fillRect(self.x + (i - 1)*8, self.y - camera_y , self.image.width, self.image.height, colors.black);
					surface:drawSurface(self.image, self.x + (i - 1) * 8, self.y - camera_y);
				end
			end;
			
			update = function(self)

				self.x = self.x + self.variables.speed;
				if self.follow then 
					self.follow.x = self.follow.x + self.variables.speed 
					if not self.follow.following then
						self.follow.x = math.floor(self.follow.x / 8) * 8; 
						self.follow = nil;
					end
				
				end
				if self.x >= 88 then self.x = -self.variables.count*8 end
				if self.x < -self.variables.count*8 then self.x = 88 end
			end;
			
			handle_events = function(self, e)
				
			end
		};
		["turtle"] = {
			variables = { speed = 0; count = 0; };
			color_maps = {};
			images = {};
			state = 1;
			substate = 0;
			temp = 1;
			timer = 0;

			init = function(self, working_path, surface_api, attribute_renderer)
				self.surface = surface;
				self.attribute_renderer = attribute_renderer;
				
				self.color_maps[1] = self.attribute_renderer.load_colormap(working_path .. "/data/gfx/entities/turtle_cm.nfp", true);
				self.images[1] = surface_api.load(working_path .. "/data/gfx/entities/turtle.nfp");
				self.color_maps[2] = self.attribute_renderer.load_colormap(working_path .. "/data/gfx/entities/turtle_2_cm.nfp", true);
				self.images[2] = surface_api.load(working_path .. "/data/gfx/entities/turtle_2.nfp");
			end;
		
			render = function(self, surface, camera_y)
				local cur_cmap = self.color_maps[1];
				local cur_image = self.images[1];
				if self.state == 0 then
					-- normal
					cur_cmap = self.color_maps[1];
					cur_image = self.images[1];
					self.timer = self.timer + 0.1;
					if self.timer > 7 then 
						self.state = 1; 
						self.timer = 0; 
					end
				elseif self.state == 1 then
					-- blinking
					self.substate = self.substate + 0.5;
					if self.substate >= 1 then
						self.temp = ((self.temp == 1) and 2 or 1)
						self.substate = 0;
					end
					cur_cmap = self.color_maps[self.temp];
					cur_image = self.images[self.temp];
					self.timer = self.timer + 0.2;
					if self.timer > 2 then
						self.substate = 3;
						self.state = 2;
						self.timer = 0;
					end
				else
					-- gone
					self.timer = self.timer + 0.1;
					if self.timer >= 2 then
						self.substate = self.substate - 1;
						self.timer = 0;
					end
					if self.substate == 3 then return; end
					if self.substate == 0 then self.state = 0; self.substate = 0; self.timer = 0; return; end
					if not self.color_maps[self.substate] then error(self.substate) end
					cur_cmap = self.color_maps[self.substate];
					cur_image = self.images[self.substate];
					
					return;
				end
				for i = 1, self.variables.count do
					self.attribute_renderer:draw_colormap(math.floor((self.x + (i - 1) * 8) / 2), 
														  math.floor((self.y - camera_y) / 3), 
														  cur_cmap);
					surface:fillRect(self.x + (i - 1)*8, self.y - camera_y , cur_image.width, cur_image.height, colors.black);
					surface:drawSurface(cur_image, self.x + (i - 1) * 8, self.y - camera_y);
				end
			end;
			
			update = function(self)
				self.x = self.x - self.variables.speed;
				if self.follow then 
					self.follow.x = self.follow.x - self.variables.speed 
					if not self.follow.following then
						self.follow.x = math.floor(self.follow.x / 8) * 8; 
						self.follow = nil;
					end
				
				end
				if self.x >= 88 then self.x = -self.variables.count*8 end
				if self.x < -self.variables.count*8 then self.x = 88 end
			end;
			
			handle_events = function(self, e)
				
			end
		};
	};
	solid_tiles = {
		-1; 2; 6;
	};
	deadly_tiles = {
		4;
	};
}