local working_path = shell.resolve(".");
local surfapi = dofile(working_path .. "/data/apis/surface.lua");

local width, height = term.getSize();
darker_colors = {
	[colors.white] = colors.lightGray;
	[colors.orange] = colors.brown;
	[colors.magenta] = colors.purple;
	[colors.lightBlue] = colors.blue;
	[colors.yellow] = colors.orange;
	[colors.lime] = colors.green;
	[colors.pink] = colors.magenta;
	[colors.gray] = colors.black;
	[colors.lightGray] = colors.gray;
		[colors.cyan] = colors.lightGray;
		[colors.purple] = colors.lightGray;
		[colors.blue] = colors.gray;
	[colors.brown] = colors.gray;
		[colors.green] = colors.gray;
		[colors.red] = colors.gray;
	[colors.black] = colors.black;
};
darken_color = function(color)
	return darker_colors[color];
end;

local gui = {
	
	remove_element = function(self, name)
		self.elements[name] = nil;
	end;
	
	find_element = function(self, name)
		return self.elements[name];
	end;
	
	count_elements = function(self, type)
		if not type then return #self.elements end
		local count = 0;
		for _, v in pairs(self.elements) do
			if v.type == type then count = count + 1 end
		end
		return count;
	end;
	
	add_label = function(self, name, text, x, y, backcolor, textcolor)
		self.elements[name] = {
			type = "label";
			x = x or 0;
			y = y or 0;
			text = text or "";
			backcolor = backcolor;
			textcolor = textcolor or colors.white;
		};

		return name;
	end;

	add_rectangle = function(self, name, x, y, width, height, backcolor, textcolor, character)
		self.elements[name] = {
			type = "rectangle";
			x = x or 0;
			y = y or 0;
			width = width or 0;
			height = height or 0;
			backcolor = backcolor or colors.black;
			textcolor = textcolor;
			character = character or " ";
		};
		return name;
	end;
	
	fade_level = 0;
	fade_index = 0;
	fade_amount = 0;
	fade = false;
	fade_handler = nil;
	fade_handler_arguments = {};
	fade_amount_max = 0;
	fade_to_black = function(self, end_handler, arguments, speed, threshold)
		self.fade = true;
		self.fade_handler = end_handler;
		self.fade_handler_arguments = arguments;
		self.fade_amount = speed;
		self.fade_amount_max = threshold or 1;
		self:_fade()
	end;
	_fade = function(self)
		self.fade_index = self.fade_index + self.fade_amount;
		if (self.fade_index >= self.fade_amount_max) then
			local c = 0;
			for k, v in pairs(self.elements) do
				c = c + 1
				v.backcolor = darken_color(v.backcolor);
				v.textcolor = darken_color(v.textcolor);
				if (v.backcolor == colors.black) and (v.textcolor == colors.black) then 
					self.fade_level = self.fade_level + 1;
				end
			end;

			if self.fade_level >= c then
				self.fade_handler(unpack(self.fade_handler_arguments));
				self.fade = false;
				self.elements = {};
			end
			self.fade_index = 0;
		end
	end;
	elements = {};
}

local main_surface = surfapi.create(width, height);
local small_surface = surfapi.create(width * 2, height * 3, colors.black, colors.white);
local game = dofile(working_path .. "/data/apis/game.lua")(working_path, gui, small_surface);


local flr = math.floor;
local timer = os.startTimer(game.timer_speed);
while game.running do
	local e = {os.pullEvent()};
	game:handle_events(e);
	if e[1] == "timer" then
		if e[2] == timer then
			game:update(gui);
			if gui.fade then gui:_fade() end

			small_surface:clear(colors.black);
			game:render(gui);

			main_surface:clear(colors.black);

			main_surface:drawSurfaceSmall(small_surface, 0, 0);
			
			for k, v in pairs(gui.elements) do
				if v.type == "label" then
					main_surface:drawString(v.text, flr(v.x), flr(v.y), v.backcolor, v.textcolor)
				elseif v.type == "rectangle" then
					main_surface:fillRect(flr(v.x), flr(v.y), flr(v.width), flr(v.height), v.backcolor, v.textcolor, v.character)
				end
			end

			main_surface:output();
			
			timer = os.startTimer(game.timer_speed);
		end
	end
end
term.setCursorPos(1, 1);
term.setBackgroundColor(colors.black);
term.setTextColor(colors.white);
term.clear();

